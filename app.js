const store = require('./store');

if (store.IsFileExists()) {
    switch (process.argv[2]) {
        case 'add':
            process.argv[3] !== undefined && process.argv[4] !== undefined ? store.AddDictionary(process.argv[3], process.argv[4]) : console.log('Missing Arguments, Please Specify the key & value after the "add" command !!');
            break;
        case 'list':
            console.log(store.GetAll());
            break;
        case 'get':
            process.argv[3] === undefined ? console.log('Invalid Arguments, Missing The Key !!') : console.log(store.GetOne(process.argv[3]));
            break;
        case 'remove':
            process.argv[3] === undefined ? console.log('Invalid Arguments, Missing The Key !!') : store.Remove(process.argv[3]);
            break;
        case 'clear':
            console.log(store.Clear());
            break;
        default:
            console.log('Invalid Command');
    }
} else {
    store.CreateFile();
}
