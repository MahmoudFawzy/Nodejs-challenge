const fs = require('fs');
const path = require('path');

const fileName = 'data.json';
const filePath = path.join(__dirname, fileName);

const isFileExists = () => fs.existsSync(filePath);

const createFile = () => fs.appendFileSync(filePath, '{}');

const readData = () => {
    const fileData = fs.readFileSync(filePath);
    return JSON.parse(fileData);
}

const getAll = () => {
    const allStoreData = readData();
    return Object.keys(allStoreData).length === 0 && allStoreData.constructor === Object ? 'There Is No Data!' : allStoreData;
};

const writeData = (data) => {
    try {
        fs.writeFileSync(filePath, JSON.stringify(data));
        return true;
    } catch (error) {
        return false;
    }
};

const addDictionary = (key, value) => {
    const storeData = readData();
    if (storeData[key]) {
        return console.log('Warning: this dictionary key already exist !!');
    } else {
        storeData[key] = value;
        writeData(storeData);
        return console.log(`Data added successfully ==> Key: ${key}, Value: ${value}`);
    }
};

const getOne = (key) => {
    const storeData = readData();
    return storeData[key] === undefined ? 'This Key Does Not Exist !!' : storeData[key];
};

const remove = (key) => {
    const storeData = readData();
    if (!storeData[key])
        return console.log('This Key Does Not Exist !!');
    delete (storeData[key]);
    writeData(storeData);
    return console.log('Data Removed Successfully !!');
}

const clear = () => {
    if (writeData({}))
        return 'All Data Cleared Successfully !!';
    return 'Error : Something Went Wrong !!';
};

module.exports.IsFileExists = isFileExists;
module.exports.CreateFile = createFile;
module.exports.AddDictionary = addDictionary;
module.exports.GetAll = getAll;
module.exports.GetOne = getOne;
module.exports.Remove = remove;
module.exports.Clear = clear;
